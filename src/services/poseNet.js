import * as posenet from '@tensorflow-models/posenet'
const imageScaleFactor = 0.5
const outputStride = 16
const flipHorizontal = true

export async function initPoseNet () {
  const net = await posenet.load()
  return net
}
export async function estimateSingle (net, media) {
  const pose = await net.estimateSinglePose(
    media,
    imageScaleFactor,
    flipHorizontal,
    outputStride
  )
  return pose
}
