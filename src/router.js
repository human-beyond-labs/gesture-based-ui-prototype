import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/webcam',
      name: 'webcam',
      // route level code-splitting
      // this generates a separate chunk (webcam.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "webcam" */ './views/Webcam.vue')
    },
    {
      path: '/tensorflow-train',
      name: 'tensorflow-train',
      // route level code-splitting
      // this generates a separate chunk (tensorflow-train.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "tensorflow-train" */ './views/TensorFlowTrain.vue')
    },
    {
      path: '/pose-from-webcam',
      name: 'pose-from-webcam',
      // route level code-splitting
      // this generates a separate chunk (pose-from-webcam.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "pose-from-webcam" */ './views/PoseFromWebcam.vue')
    },
    {
      path: '/hand-track',
      name: 'hand-track',
      // route level code-splitting
      // this generates a separate chunk (hand-track.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "hand-track" */ './views/HandTrack.vue')
    },
    {
      path: '/three-on-vue',
      name: 'three-on-vue',
      // route level code-splitting
      // this generates a separate chunk (three-on-vue.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "three-on-vue" */ './views/ThreeOnVue.vue')
    },
    {
      path: '/gesture-three',
      name: 'gesture-three',
      // route level code-splitting
      // this generates a separate chunk (gesture-three.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "gesture-three" */ './views/GestureThree.vue')
    }
  ]
})
